# Attribution Source Code License v1

This code is provided as-is, that is to say, without liability or warranty. 
Original authors are not held responsible for any damages or other claims said.

You may copy, modify, contribute, and distribute, for public or private use, **as long as the following are followed:**
- You may not use the code for commercial (any sort of profit) use.
- You must clearly specify that original code are not yours: the developers of both must be credited.
- You may not distribute assets used to run the software not directly provided by the repository (other than unique, modded assets).
- This license must be in all modified copies of source code, and all forks must follow this license.
