# No decompile EULA V1.0

By exercising the Licensed Rights (defined below), You accept and agree to be bound by the terms and conditions of this no decompile EULA ("End user license agreement"). To the extent this EULA may be interpreted as a contract, You are granted the Licensed Rights in consideration of Your acceptance of these terms and conditions, and the Licensor grants You such rights in consideration of benefits the Licensor receives from making the Licensed Material available under these terms and conditions.

## Section 1 – Definitions.

a. Adapted Material means material subject to Copyright and Similar Rights that is derived from or based upon the Licensed Material and in which the Licensed Material is translated, altered, arranged, transformed, or otherwise modified in a manner requiring permission under the Copyright and Similar Rights held by the Licensor. For purposes of this EULA, where the Licensed Material is a musical work, performance, or sound recording, Adapted Material is always produced where the Licensed Material is synched in timed relation with a moving image.

b. Copyright and Similar Rights means copyright and/or similar rights closely related to copyright including, without limitation, performance, broadcast, sound recording, and Sui Generis Database Rights, without regard to how the rights are labeled or categorized. For purposes of this EULA, the rights specified in Section 2(b)(1)-(2) are not Copyright and Similar Rights.

c. Effective Technological Measures means those measures that, in the absence of proper authority, may not be circumvented under laws fulfilling obligations under Article 11 of the WIPO Copyright Treaty adopted on December 20, 1996, and/or similar international agreements.

d. Exceptions and Limitations means fair use, fair dealing, and/or any other exception or limitation to Copyright and Similar Rights that applies to Your use of the Licensed Material.

e. Licensed Material means the artistic or literary work, database, or other material to which the Licensor applied this EULA.

f. Licensed Rights means the rights granted to You subject to the terms and conditions of this EULA, which are limited to all Copyright and Similar Rights that apply to Your use of the Licensed Material and that the Licensor has authority to license.

g. Licensor means the individual(s) or entity(ies) granting rights under this EULA.

h. NonCommercial means not primarily intended for or directed towards commercial advantage or monetary compensation. For purposes of this EULA, the exchange of the Licensed Material for other material subject to Copyright and Similar Rights by digital file-sharing or similar means is NonCommercial provided there is no payment of monetary compensation in connection with the exchange.

i. Share means to provide material to the public by any means or process that requires permission under the Licensed Rights, such as reproduction, public display, public performance, distribution, dissemination, communication, or importation, and to make material available to the public including in ways that members of the public may access the material from a place and at a time individually chosen by them.

j. You means the individual or entity exercising the Licensed Rights under this EULA. Your has a corresponding meaning.

## Section 2 – Scope.

#### a. License grant.

1. Subject to the terms and conditions of this EULA, the Licensor hereby grants You a limited, personal, non-sublicensable, non-exclusive, revocable license to exercise the Licensed Rights in the Licensed Material to:

a.Use the software for personal, NonCommercial use; and

b.Use the software according to the EULA.

2. Exceptions and Limitations. For the avoidance of doubt, where Exceptions and Limitations apply to Your use, this EULA does not apply, and You do not need to comply with its terms and conditions.

3. Term. The term of this EULA is specified in Section 6(a).

4. Media and formats; technical modifications not allowed. The Licensor does not  authorizes You to exercise the Licensed Rights in all media and formats whether now known or hereafter created. created. You can't decompile, disamble, reverse engineering this software, without authorization of the software rights owner.

b. Other rights.

1. Moral rights, such as the right of integrity, are not licensed under this EULA, nor are publicity, privacy, and/or other similar personality rights; however, to the extent possible, the Licensor waives and/or agrees not to assert any such rights held by the Licensor to the limited extent necessary to allow You to exercise the Licensed Rights, but not otherwise.

2. Patent and trademark rights are not licensed under this EULA.

## Section 3 – License Conditions.

Your exercise of the Licensed Rights is expressly made subject to the following conditions.

a. You may only use this software for your Personal, NonCommercial use. If you use the software commercially or externally, it will result into a termination in section 5.

b. The licensor may apply Technological measures in the work and you can not avoid it.

c. You may not distribute the work unless authorized by the licensor.

d. You can't use the work for sexual, unlawful, discriminatory purposes.

# Section 4 – Disclaimer of Warranties and Limitation of Liability.

a. Unless otherwise separately undertaken by the Licensor, to the extent possible, the Licensor offers the Licensed Material as-is and as-available, and makes no representations or warranties of any kind concerning the Licensed Material, whether express, implied, statutory, or other. This includes, without limitation, warranties of title, merchantability, fitness for a particular purpose, non-infringement, absence of latent or other defects, accuracy, or the presence or absence of errors, whether or not known or discoverable. Where disclaimers of warranties are not allowed in full or in part, this disclaimer may not apply to You.

b. To the extent possible, in no event will the Licensor be liable to You on any legal theory (including, without limitation, negligence) or otherwise for any direct, special, indirect, incidental, consequential, punitive, exemplary, or other losses, costs, expenses, or damages arising out of this EULA or use of the Licensed Material, even if the Licensor has been advised of the possibility of such losses, costs, expenses, or damages. Where a limitation of liability is not allowed in full or in part, this limitation may not apply to You.

c. The disclaimer of warranties and limitation of liability provided above shall be interpreted in a manner that, to the extent possible, most closely approximates an absolute disclaimer and waiver of all liability.

## Section 5 – Term and Termination.

a. This EULA applies for the term of the Copyright and Similar Rights licensed here. However, if You fail to comply with this EULA, then Your rights under this EULA terminate automatically.

b. Where Your right to use the Licensed Material has terminated under Section 5(a), it reinstates:

1. automatically as of the date the violation is cured, provided it is cured within 30 days of Your discovery of the violation; or

2. upon express reinstatement by the Licensor.

c. Sections 1, 4, 5, 6, and 7 survive termination of this EULA.

## Section 6 – Other Terms and Conditions.

a. The Licensor shall be bound by any additional or different terms or conditions communicated by You unless expressly agreed.

c. Any arrangements, understandings, or agreements regarding the Licensed Material not stated herein are separate from and independent of the terms and conditions of this EULA.

## Section 7 – Interpretation.

a. The licensor may revoque any of the rights of this EULA, and change it to other EULA or license freely, but can not change the license text without previous authorization of the creator of the license document (Alejo Fernandez).












































